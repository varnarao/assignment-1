/* eslint-disable react/prop-types */
import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import HandleModal from '../../components/editModal'

const TblDisplay = (props) => {
  const searchVal = props.search ? props.search : ''
  return (
    <div >
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="right">Description</TableCell>
              <TableCell align="right">Category</TableCell>
              <TableCell align="right">Edit</TableCell>
              <TableCell align="right">Delete</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.row.filter(row => row.title.includes(searchVal)).map(row => (
              <TableRow key={row.id}>
                <TableCell component="th">{row.title}</TableCell>
                <TableCell align="right">{(row.description.length > 11 ? row.description.substr(0, 11) + '...' : row.description)}</TableCell>
                <TableCell align="right">{row.category}</TableCell>
                <TableCell align="right"><EditIcon onClick={() => props.clicked(row.id, row.title, row.description, row.category)}/></TableCell>
                <TableCell align="right"><DeleteIcon /></TableCell>
                <TableCell align="right"></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <HandleModal name = {props.name} nameChange = {props.nameChange} desp = {props.desp} despChange = {props.despChange} catgChange = {props.catgChange} open={props.open} close={props.close}/>
    </div>
  )
}
export default TblDisplay
