export default function reducer (state = {
  products: [],
  fetching: false,
  fetched: false,
  done: false,
  name: null,
  desp: null,
  id: null,
  category: null,
  open: false,
  error: null
}, action) {
  switch (action.type) {
    case 'FETCH_PRODUCT_PENDING': {
      return { ...state, fetching: true }
    }
    case 'FETCH_PRODUCT_REJECTED': {
      return { ...state, fetching: false, error: action.payload }
    }
    case 'FETCH_PRODUCT_FULFILLED': {
      return {
        ...state,
        fetching: false,
        fetched: true,
        done: true,
        products: action.payload
      }
    }
    case 'CHANGE_NAME': {
      return {
        ...state,
        name: action.payload
      }
    }
    case 'CHANGE_DESP': {
      return {
        ...state,
        desp: action.payload.id
      }
    }
    case 'CHANGE_CATEGORY': {
      return {
        ...state,
        category: action.payload
      }
    }
    case 'MODAL_OPEN' : {
      const { id, name, desp, category } = action.payload
      return {
        ...state,
        id: id,
        name: name,
        desp: desp,
        category: category,
        open: true
      }
    }
    case 'MODAL_CLOSE' : {
      return {
        ...state,
        open: false
      }
    }
    case 'EDIT_DATA_FULFILLED': {
      return {
        ...state,
        fetching: false,
        fetched: true,
        done: true,
        products: action.payload
      }
    }
    default : {
      return state
    }
  }
}
