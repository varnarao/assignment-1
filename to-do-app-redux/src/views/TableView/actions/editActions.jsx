export function changeName (name) {
  return {
    type: 'CHANGE_NAME',
    payload: name
  }
}
export function changeDesp (desp) {
  return {
    type: 'CHANGE_DESP',
    payload: desp
  }
}
export function changeCategory (category) {
  return {
    type: 'CHANGE_CATEGORY',
    payload: category
  }
}
export function ModalOpenHandler (id, name, desp, category) {
  return {
    type: 'MODAL_OPEN',
    payload: { id, name, desp, category }
  }
}
export function ModalcloseHandler () {
  return {
    type: 'MODAL_CLOSE'
  }
}
export function putData () {
  return {
    type: 'EDIT_DATA'
  }
}
