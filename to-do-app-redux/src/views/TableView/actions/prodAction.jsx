import { fetchData } from '../../../apiCallMethods/apiCallMethods'
import URlConstants from '../../../constants/apiConstants'
export function getProducts (done) {
  return {
    type: 'FETCH_PRODUCT',
    payload: fetchDatas(),
    done
  }
}

async function fetchDatas () {
  const res = await fetchData(`${URlConstants.baseUrl}/${URlConstants.taskApi}`)
  return res
}
