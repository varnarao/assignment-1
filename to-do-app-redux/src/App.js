import React from 'react'
import './App.css'

import Menu from './components/menuDrawer'

class App extends React.Component {
  render () {
    return (
      <div className="App">
        <Menu />
      </div>
    )
  }
}

export default App
