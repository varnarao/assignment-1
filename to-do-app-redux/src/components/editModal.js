/* eslint-disable react/prop-types */
import React from 'react'
import Modal from '@material-ui/core/Modal'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import styles from '../styles/tableStyle'

const editModal = (props) => {
  const classes = styles()
  return (
    <Modal
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      open= {props.open}
      onClose={props.close}
    >
      <div className={classes.paper}>
        <h2 id="simple-modal-title">Edit Components</h2>
        <form noValidate autoComplete="off" id="simple-modal-description">
          <div className={classes.root}>
            <TextField id="name" label="Name" variant="outlined" value ={props.name} onChange={(event) => props.nameChange(event)} /><br/><br/>
            <TextField id="Description" multiline rows="4" label="Description" value = {props.desp} onChange={(event) => props.despChange(event)} variant="outlined" /><br/><br/>
            <TextField id="category" label="Category" value={props.category} onChange={(event) => props.catgChange(event)} variant="outlined" /><br/><br/>
            <Button variant="contained" color="primary">Submit</Button>
          </div>
        </form></div>
    </Modal>
  )
}
export default editModal
