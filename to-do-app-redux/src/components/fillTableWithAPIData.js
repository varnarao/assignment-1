import React from 'react';
import Tbldisp from '../views/TableView/tableProductInfoView'
import { connect } from 'react-redux'
import { getProducts } from '../views/TableView/actions/prodAction'
import { changeName,ModalOpenHandler,ModalcloseHandler,changeDesp,changeCategory } from '../views/TableView/actions/editActions'
class  FillTable extends React.Component {
  componentDidMount() {
    this.props.dispatch(getProducts())
  }
  changeNameHandler = (event) => {
    this.props.dispatch(changeName(event.target.value))
  }
  changeDespHandler = (event) => {
    this.props.dispatch(changeDesp(event.target.value))
  }
  changeCategoryHandler = (event) => {
    this.props.dispatch(changeCategory(event.target.value))
  }
  clickHandler = (id,name,desp,category) => {
    this.props.dispatch(ModalOpenHandler(id,name,desp,category))
  }
  closedHandler= () => {
    this.props.dispatch(ModalcloseHandler())
  }
  render(){
    const disp = this.props.done ? <Tbldisp row = {this.props.prod.data} name = {this.props.name} nameChange = {this.changeNameHandler} desp={this.props.desp} despChange = {this.changeDespHandler} category = {this.props.category} catgChange = {this.props.changeCategoryHandler} open={this.props.open} clicked={this.clickHandler} close={this.closedHandler} /> : "Loading..."
  return (
    <div className="App">
      {disp}
    </div>
  );
}
}
const mapStateToProps =  state => {
  return{
    prod: state.products,
    done : state.done,
    name : state.name,
    open : state.open,
    desp : state.desp,
    category : state.category
  }
}
export default connect(mapStateToProps)(FillTable);
