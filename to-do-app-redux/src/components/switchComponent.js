import React from 'react';
import styles from '../styles/tableStyle'
import {BrowserRouter as Router,Switch,Route,Link
} from "react-router-dom";


export default function switchcomp(props){
    return(
        
        <Switch>
                {
                    props.routes.map((route,index)=>(
                        <Route key={index}
                         path={route.path}
                         exact={route.exact}
                         component={route.component}/>
                    ))
                }
      </Switch>
    )
}