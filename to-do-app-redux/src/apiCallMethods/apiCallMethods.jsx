import axios from 'axios'

export function fetchData (url) {
  return axios.get(url)
}

export function putData (url) {
  return axios.put(url)
}
