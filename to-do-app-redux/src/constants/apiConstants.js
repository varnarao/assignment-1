const apiConstants = {
  baseUrl: 'https://5e410e092001b900146b9f07.mockapi.io/api/v1',
  taskApi: '/TodoList'
}
export default apiConstants
