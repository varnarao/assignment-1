import React,{ Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ValidationComponent from './ValidationComponent/ValidationComponent';
import CharComponent from './CharComponents/CharComponents';

class App extends Component { 
  state = {
    countCha : 0,
    Charval : []
  }
  countChar = (event) => {
    const charValue  = event.target.value
    //alert(charValue)
    let countChars = 0;
    for(let i=0;i<charValue.length;i++){
      countChars++;
    }
    
    this.setState({countCha : countChars})
    //alert(countChars);
    const c = [...charValue]
    this.setState({Charval : c})
    //alert("char = "+charValue);
    //alert(this.state.Charval)
    
    
    
  }

  deleteCharHandler = (index) => {
    const charPos = [...this.state.Charval]

    charPos.splice(index,1)
    let newcount = this.state.countCha
    newcount--;
    this.setState({Charval:charPos,
      countCha:newcount})
  }
  render(){
    const style = {
      border : "1px solid black",
      height: "10px",
      width : "10px"
    };
    return (
      <div className="App">
        <input type="text" onChange={this.countChar} value={this.state.Charval.join('')}/>
        <ValidationComponent leng = {this.state.countCha} />
        {/*char1 = (
          <div>
            {
              this.state.charVal.map(c1 => {
                  return(
                    <CharComponent 
                    val = {c1}
                    />
                  )
              })
            }
          </div>
          )*/}
          
              {
                this.state.Charval.map((c1,index) => {
                  return(
                    <CharComponent val={c1}
                    click = {() => this.deleteCharHandler(index)}
                    sty = {this.style}/>
                  )
                })
              }
              
            
      </div>
    )
  }
  
}

export default App;
