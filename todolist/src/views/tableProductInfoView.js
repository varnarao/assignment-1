import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';
import styles from '../styles/tableStyle'
import HandleModal from '../components/editModal'
import {BrowserRouter as Router,Switch,Route,Link,NavLink
} from "react-router-dom";

const TblDisplay = (props) => {
    console.log("Props = "+props.search)
    const searchVal = props.search ? props.search : ''
    const classes = styles()
        return(
            <div className={classes.tbldisp}>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell align="right">Description</TableCell>
                                <TableCell align="right">Category</TableCell>
                                <TableCell align="right">Edit</TableCell>
                                <TableCell align="right">Delete</TableCell>
                                
                                
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {props.row.filter(row => row.title.includes(searchVal)).map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th">{row.title}</TableCell>
                                <TableCell align="right">{(row.description.length>11?row.description.substr(0, 11)+"...":row.description)}</TableCell>
                                <TableCell align="right">{row.category}</TableCell>
                                <TableCell align="right"><EditIcon onClick={() => props.clicked(row.id,row.title,row.description,row.category)}/></TableCell>
                                <TableCell align="right"><DeleteIcon onClick={() => props.deleteval(row.id)}/></TableCell>
                               
                            </TableRow>
                        ))}
                    </TableBody>
                    </Table>    
                </TableContainer>
                <HandleModal catg={props.catg} desp={props.desp} name={props.name} changescatg={props.changecatg} changesdesp={props.changedesp} changes={props.changed} openva={props.openval} closes={props.closed} editva ={props.editval} />
        </div>
    )
}
export default TblDisplay;