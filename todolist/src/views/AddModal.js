import React from 'react'; 
import axios from 'axios'
import Form from '../components/formComponent'
import SnackBar from '../components/snackBarComponent'

class addModal extends React.Component {
  state ={open : false,err:null}


  handleClosed = () =>{
    console.log("closed")
    this.setState({
      open : false
    })
  }
    render() {
      const ret = this.state.open===true?<SnackBar op={this.state.open} closed={this.handleClosed}/>:console.log("success")
    return(
        <div>
           {ret}
           <Form />
        </div>
    )
  }
}
export default addModal;