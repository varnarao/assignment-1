import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import clsx from 'clsx'
import {useTheme} from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import styles from '../styles/tableStyle'
import {BrowserRouter as Router,Switch,Route,Link,NavLink} from "react-router-dom";
import Listview from './listView'
import Appbar from './appBarComponent'
import SwitchComp from './switchComponent'
import Routes from './routeCompVariable'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListAltIcon from '@material-ui/icons/ListAlt';
export default function PersistentDrawerLeft(props) {
  const classes = styles();
  const theme = useTheme();
  const routes = Routes(props.changeVal)
  const [open, setOpen] = React.useState(false);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.r1}>
      <CssBaseline />
     <Appbar op={handleDrawerOpen} val={open} changed={props.changed}/>
      <Router>
      <Drawer className={classes.drawer} variant="persistent" anchor="left" open={open} classes={{paper: classes.drawerPaper,}}>
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
          <Listview />
          
      </Drawer>
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        <div className={classes.drawerHeader} />
        <SwitchComp routes={routes} op={open}/>
          
      </main>
        
      </Router>
    </div>
  );
}