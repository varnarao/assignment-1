import React from 'react'
import { withFormik } from 'formik'
import * as Yup from 'yup'
import { withStyles } from '@material-ui/core'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import { postMethodContent } from './apiCallMethods'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
const styles = () => ({
  card: {
    maxWidth: 420,
    marginTop: 50
  },
  container: {
    display: 'Flex',
    justifyContent: 'center'
  },
  actions: {
    float: 'right'
  }
})

const form = props => {
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset
  } = props

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
          <CardContent>
            <TextField
              id='title'
              label='Title'
              value={values.title}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.title ? errors.title : ''}
              error={touched.title && Boolean(errors.title)}
              margin='dense'
              variant='outlined'
              fullWidth
            />
            <TextField
              id='description'
              label='Description'
              type='description'
              value={values.description}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.description ? errors.description : ''}
              error={touched.description && Boolean(errors.description)}
              multiline
              rows='4'
              margin='dense'
              variant='outlined'
              fullWidth
            />
            <TextField
              id='category'
              label='Category'
              value={values.category}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.category ? errors.category : ''}
              error={touched.category && Boolean(errors.category)}
              margin='dense'
              variant='outlined'
              fullWidth
            />
          </CardContent>
          <Button type='submit' color='secondary'>
              SUBMIT</Button>
        </Card>
      </form>
    </div>
  )
}

const Form = withFormik({
  mapPropsToValues: ({
    title,
    category,
    description
  }) => {
    return {
      title: title || '',
      category: category || '',
      description: description || '',
    };
  },

  validationSchema: Yup.object().shape({
    title: Yup.string().required('Required'),
    category: Yup.string().required('Required'),
    description: Yup.string().required('description is required'),
  }),

  handleSubmit: (values, { setSubmitting }) => {
    postMethodContent(values)
  }
})(form)

export default withStyles(styles)(Form);