import axios from 'axios'
import ApiConst from '../constants/apiConstants'

const baseUrl = ApiConst.baseUrl
const taskApi = ApiConst.taskApi
const apiUrl = baseUrl + taskApi
console.log('Base = ', baseUrl)
export function fetchData () {
  const url = `${apiUrl}`;
  return fetch(url)
        .then(response => response.json())
        .then(data=>{
            return data        
        })
        .catch(error => console.log(error));
}

export function editData(prod,headers,ProdID){
    axios.put(`${apiUrl}/${ProdID}`, prod,headers)
    .then((response) => {
      console.log(response)
      return fetchData()
    })
    .catch(error => console.log(error));
}

export function deleteModalContent(id) {
      axios.delete(`${apiUrl}/${id}`)
  .then(res => {
      console.log(res.data)
    return fetchData()
});
    
}

export function postMethodContent(prod){
    axios.post(`${apiUrl}`,prod)
        .then((response) => {
        console.log(response)
        alert("Inserted")
        return response
    })
    .catch(error => console.log(error));
}