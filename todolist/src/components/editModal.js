import React from 'react'; 
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import styles from '../styles/tableStyle'

const editModal = (props) => {
    const classes = styles()
    console.log(props.openva)
    return(
        <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open= {props.openva}
        onClose={props.closes}
        >
        <div className={classes.paper}>
        <h2 id="simple-modal-title">Edit Components</h2>
        <form noValidate autoComplete="off" id="simple-modal-description">
            <div className={classes.root}>
            <TextField id="name" label="Name" variant="outlined" value={props.name} onChange={(event) => props.changes(event)} /><br/><br/>
            <TextField id="Description" multiline rows="4" label="Description" variant="outlined" value={props.desp} onChange={(event) => props.changesdesp(event)} /><br/><br/>
            <TextField id="category"  label="Category" variant="outlined" value={props.catg} onChange={(event) => props.changescatg(event)} /><br/><br/>
            <Button variant="contained" color="primary"
                    onClick={() => props.editva(props.name, props.desp,props.catg)}>Submit</Button> 
            </div>
        </form>
    </div>
    </Modal>
    )
}
export default editModal;