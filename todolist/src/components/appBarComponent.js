import React from 'react'
import clsx from 'clsx'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import styles from '../styles/tableStyle'
import Search from './searchComponents'

const drawerComp = (props) => {
  const classes = styles()
  return (
    <AppBar>
      <Toolbar>
        <IconButton color="inherit" aria-label="open drawer" onClick={props.op} edge="start" className={clsx(classes.menuButton, props.val && classes.hide)}>
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" noWrap>
        </Typography>
        <Search changed={props.changed}/>
      </Toolbar>
    </AppBar>
  )
}
export default drawerComp
