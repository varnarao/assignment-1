import React from 'react';
import ModalAdd from '../views/AddModal';
import ToDo from '../components/fillTableWithAPIData'

const routes = (props) => {
        return [
            {
                path:"/",
                exact:true,
                component: props ?  () => <ToDo routeprop={props}/> : ToDo
            },
            {
                path:"/Add",
                component:ModalAdd
            },
        ]
    }
export default routes

