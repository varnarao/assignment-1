import React from 'react';
import Tbldisp from '../views/tableProductInfoView'
import {fetchData} from './apiCallMethods'
import {editData} from './apiCallMethods'
import {deleteModalContent} from './apiCallMethods'

class FillTable extends React.Component{
    state = {
        Products : [],
        ModalOpen : false,
        ProdID : null,
        Name : null,
        Category : null,
        Desp : null,
        done : false,
        searchval : this.props.routeprop
    }

    componentDidMount(){
       this.fetchData()
    }

     handleModalopen = (id,name,desp,catg) => {
        this.setState({
        ModalOpen : true,
        ProdID:id, Name:name,Desp:desp,Category:catg
        })
      }
      
      handleCloseModal = () => {
        const index = this.state.Products.findIndex(p => {
          return p.id === this.state.ProdID})
        
        this.setState({
            ModalOpen : false
        })
        
      }
      changeHandler = (event) => {
        const index = this.state.Products.findIndex(p => {
            return p.id === this.state.ProdID})
         
          this.setState({
            Name : event.target.value
          })
        
    }

    changeDespHandler = (event) => {
      const index = this.state.Products.findIndex(p => {
          return p.id === this.state.ProdID})
       
        this.setState({
          Desp : event.target.value
        })   
  }

  changeCatgHandler = (event) => {
    const index = this.state.Products.findIndex(p => {
        return p.id === this.state.ProdID})
     
      this.setState({
        Category : event.target.value
      })
  }  
  editModalContent = (Name,Description,Category) => {
    let axiosConfig = {
      headers: {
          'Content-Type': 'application/json;charset=UTF-8',
      }
    };
    const prod = {title:Name,description:Description,category:Category}
      editData(prod,axiosConfig,this.state.ProdID)
      this.handleCloseModal()
      this.setState({done:false})
      setTimeout(() => this.fetchData(),2000)    
    }
    
  deleteModalContent = (id) => {
      let r = window.confirm("Are you sure?");
      if (r == true) {
        deleteModalContent(id)
        this.setState({done:false})
        setTimeout(() => this.fetchData(),2000)
    
      } else {
        alert("OK!")
      }
  }
    
    async fetchData(){
      let response = await fetchData()
      const data = await response
      this.setState({ Products: data,done:true });
    }
    render(){
     
      const loadval = (this.state.done===true) ? <Tbldisp row={this.state.Products} 
      deleteval = {this.deleteModalContent} name={this.state.Name} search ={this.state.searchval} catg = {this.state.Category} changecatg={this.changeCatgHandler}
      editval = {this.editModalContent} changed={this.changeHandler} desp = {this.state.Desp} changedesp={this.changeDespHandler}
      closed = {this.handleCloseModal} clicked={this.handleModalopen} openval = {this.state.ModalOpen} /> : <div className="Load"> <h4>Loading...</h4></div>
      return(
        
            <div>
                {loadval}
            </div>
        )
    }
}
export default FillTable;