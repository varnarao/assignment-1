import React from 'react'
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import Typography from '@material-ui/core/Typography'
import styles from '../styles/tableStyle'

const searchval = (props) => {
  const classes = styles()
  return (
    <div className={classes.rootsearch}>
          
        <Typography className={classes.title} variant="h6" noWrap />
        <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <div></div>
            <InputBase
              placeholder="Search…"
              onChange={(event) => props.changed(event)}
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput
              }}
              inputProps={{ "aria-label": "search" }}
            />
          </div>
          
          </div>

    )
}

export default searchval;