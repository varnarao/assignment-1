import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListAltIcon from '@material-ui/icons/ListAlt';
import styles from '../styles/tableStyle'
import {BrowserRouter as Router,Switch,Route,Link,NavLink
} from "react-router-dom";

export default function listV(){
    const classes = styles()
    return (
        <List>
        <NavLink exact activeStyle={{
    fontWeight: "bold",
    color: "red"}} className={classes.listStyle} to="/">
         <ListItem button >
             <ListItemIcon>
             <ListAltIcon />
             </ListItemIcon>
             <ListItemText primary="List" />
             
         </ListItem>
         </NavLink>
         <NavLink activeStyle={{
    fontWeight: "bold",
    color: "red"}} className={classes.listStyle} to="/Add">
         <ListItem button>
             <ListItemIcon>
             <ListAltIcon />
             </ListItemIcon>
             <ListItemText primary="Add" />
             
         </ListItem>
         </NavLink>
         </List>
    )
}