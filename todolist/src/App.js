import React from 'react';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import Menu from './components/menuDrawer'
import Table from './components/apiCallMethods'


class App extends React.Component {
  state ={
    SearchValue : null,
    load : false
  }
  changeHandler=(event) =>{
    if(event.target.value !== '')
      this.setState({SearchValue : event.target.value,load : true })
    else
    this.setState({SearchValue : null,load: false })
    //alert("change")
  }
  render(){
    return (
      <div className="App">
        <Menu changed={this.changeHandler} changeVal={this.state.SearchValue}/>
      </div>
    );
  } 
}

export default App;
