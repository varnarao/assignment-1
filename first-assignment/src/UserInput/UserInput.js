import React from 'react';

const userInput = (props) => {
    return (
        <div>
            <p>{props.name}</p>
            <input type="text" onChange={props.changed}/>
        </div>
        
    );
    
};

export default userInput; 