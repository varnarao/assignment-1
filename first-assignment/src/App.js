import React,{ Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { render } from '@testing-library/react';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

class App extends Component {
  state = {
    UName : [{name:"varsha"},
            {name : "pavi"}]
  };
  NameChangeHandler = (event) => {
    this.setState(
      {
        UName : [{name:event.target.value},
            {name : "pavi"}]
      }
    );
  }
  render(){
    return (
      <div className="App">
        <h1> HELLO </h1>
        <UserInput 
        changed = {this.NameChangeHandler}/>
        <UserOutput 
        name = {this.state.UName[0].name}
        age = "23"
        />
      </div>
    );

  }
  
}

export default App;
