import React from 'react';
import logo from './logo.svg';
import './App.css';


class Square extends React.Component {
  
  
  render() {
    return (
      <button className="square" onClick={this.props.clicked}>
        {this.props.value}
      </button>
    );
  }
}

class Board extends React.Component {
  

  renderSquare(i) {
    return <Square value={this.props.squares[i]} clicked={() => this.props.clicked(i)}/>;
  }

  render() {
    //const status = 'Next player: X';

    return (
      <div>
        
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class App extends React.Component {
  state = {
    history:[{
      squares : Array(9).fill(null)
    }],
    SelectState : 0,
    IsSelected : true
  }

  /*jumpto(index){
    const hist = this.state.history.slice()
    const current = hist[index]
    const squares = current.squares.slice()
    this.setState({
      history: [{
        squares : squares
      }]
    })
  }*/

  jumpto = (index) => {
    this.setState({
      SelectState : index,
      IsSelected: (index % 2) === 0
    })
  }

  clickEventHandler = (i) =>{
    const history = this.state.history.slice(0, this.state.SelectState + 1);
    const current = history[history.length -1]
    const squares = current.squares.slice();
    if(calculateWinner(squares) || squares[i])
      return
    if(this.state.IsSelected)
      squares[i] = 'X';
    else
      squares[i] = 'O';
    const val = this.state.IsSelected
    
    this.setState({
      history : history.concat([{
        squares : squares
      }]),
      SelectState:history.length,
      IsSelected : !val
    } );
  }

  render() {
    const history = this.state.history.slice()
    const current = history[this.state.SelectState]
    const winner = calculateWinner(current.squares)
    let status;
    if(winner){
      status = "Winner = "+winner;
    }else{
      status = (this.state.IsSelected === true) ? 'Next - Player X' : 'Next - Player O'
    }
    const buttonval = history.map((value,index) => {
      const val = (index != 0)? 'Go back to move # '+index : 'Go back to start'
      return(
        <button onClick={() => this.jumpto(index)}> {val} </button>
      )
    })
    return (
      <div className="game">
        
        <div className="game-board">
          <Board squares={current.squares} clicked={(i) => this.clickEventHandler(i)}/>
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{buttonval}</ol>
        </div>
      </div>
    );
  }
}
function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

export default App;
